package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	rand.Seed(time.Now().UnixNano())

	intValue := rand.Int()
	stringValue := IntToString(intValue)
	fmt.Printf("%d is %T => %s is %T\n", intValue, intValue, stringValue, stringValue)
	fmt.Printf("%s is %T => %d is %T\n", stringValue, stringValue, StringToInt(stringValue), StringToInt(stringValue))

	int32Value := rand.Int31()
	fmt.Printf("%d is %T => %f is %T\n", int32Value, int32Value, Int32ToFloat32(int32Value), Int32ToFloat32(int32Value))

	int64Value := rand.Int63()
	fmt.Printf("%d is %T => %f is %T\n", int64Value, int64Value, Int64ToFloat64(int64Value), Int64ToFloat64(int64Value))

	stringValue = "true"
	fmt.Printf("%s is %T => %t is %T\n", stringValue, stringValue, StringToBool(stringValue), StringToBool(stringValue))

	stringValue = "T"
	fmt.Printf("%s is %T => %t is %T\n", stringValue, stringValue, StringToBool(stringValue), StringToBool(stringValue))

	stringValue = "False"
	fmt.Printf("%s is %T => %t is %T\n", stringValue, stringValue, StringToBool(stringValue), StringToBool(stringValue))

	var runeValue rune
	runeValue = 'a'
	fmt.Printf("%c is %T => %s is %T", runeValue, runeValue, RuneToString(runeValue), RuneToString(runeValue))

}
