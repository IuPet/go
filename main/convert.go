package main

import (
	"strconv"
)

func IntToString(i int) string {
	return strconv.Itoa(i)
}
func Int32ToFloat32(i int32) float32 {
	return float32(i)
}
func Int64ToFloat64(i int64) float64 {
	return float64(i)
}
func StringToBool(s string) bool {
	var result, _ = strconv.ParseBool(s)
	return result
}
func StringToInt(s string) (result int) {
	result, _ = strconv.Atoi(s)
	return
}
func RuneToString(r rune) string {
	return strconv.QuoteRune(r)
}
